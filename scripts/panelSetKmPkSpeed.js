import React from 'react';
import { View, StyleSheet } from 'react-native';
import PickerCustom from './customPicker';

export default function PanelSetKmPkSpeed(props) {
  let listPickerItemsKM = props.region.map((e) => e.id/1000);
  let listPickerItemsPK = [...Array(10)].map((e,i) => i + 1);
  let lisetPickerItemsSpeed = [...Array(50)].map((e,i)=>i*5);
  return(
    <View style={styles.main}>
      <PickerCustom label = "КМ" onValueChange={props.setKm} listPickerItems = {listPickerItemsKM}
      stylesContainer = {styles.container} stylesPicker={styles.picker} stylesText={styles.text} />
      <PickerCustom label = "ПК" onValueChange={props.setPk} listPickerItems = {listPickerItemsPK} 
      stylesContainer = {styles.container} stylesPicker={styles.picker} stylesText={styles.text}/>
      <PickerCustom label = "Скорость" onValueChange={props.setSpeed} listPickerItems = {lisetPickerItemsSpeed} 
      stylesContainer = {styles.container} stylesPicker={styles.picker} stylesText={styles.text}/>
  </View>
  )
}


const styles = StyleSheet.create({
  main: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: 'space-between',
  },  
  container: {
    flex: 1,
    flexDirection: 'column-reverse'
  },
  picker: {
    borderStyle: "solid",
    borderWidth: 2,
    borderRadius: 15,
    textAlign: "center",
    marginHorizontal: '5%',
    fontSize: 20
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: '5%'
  }
  
})