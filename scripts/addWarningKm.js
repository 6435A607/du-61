import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { addWarningKm } from './actions';
import PickerCustom from './customPicker';


function AddWarningKm(props) {
  const arrayWarningKm = [...Array(11)].map((e,i) => `${i} км`); //MAGIC NUMBER!!!!!!!!!!!!!

  return (
    <>
      <View >
        <PickerCustom  
        label={"Предупредить за:"} onValueChange={props.setWarningKM} listPickerItems={arrayWarningKm}
        stylesContainer = {styles.container} stylesPicker={styles.picker} stylesText={styles.text} />
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'column-reverse',
    width: '75%',
    alignSelf: 'center',
  },
  picker: {
    borderStyle: "solid",
    borderWidth: 2,
    borderRadius: 15,
    marginHorizontal: '5%',
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: '5%'
  }
})

mapStateToProps = (state) => {
  return {
    warningKm: state.warningKm
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    addWarningKm: (warningKm) => {
      dispatch(addWarningKm(warningKm));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddWarningKm);