import React, {useState} from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { addIsThere } from './actions';
import  RadioForm, {RadioButton, RadioButtonLabel, RadioButtonInput } from 'react-native-simple-radio-button';


function AddIsThere(props) {
  const [initialIndex, setInitialIndex] = useState(0);
  const radioProps = [
    { label: "Туда", value: -1 },
    { label: "Обратно", value: 1}
  ];

  const handleOnPress = (value) => {
    props.setIsThere(value);
    let index = radioProps.findIndex(item => item.value == value);
    setInitialIndex(index);
  }

  return(
    <View>
      <RadioForm
        formHorizontal={true}
        animation={false}
        style={styles.container}
      >
        {
          radioProps.map((obj, index) => (
            <RadioButton labelHorizontal={true} key={index}>
              <RadioButtonInput
                obj={obj}
                index={index}
                isSelected={initialIndex === index}
                onPress = { handleOnPress }
                borderWidth = {3}
                buttonInnerColor = {'deepskyblue'}
                buttonOuterColor = { 'deepskyblue' }
                buttonSize = {30} 
                buttonOuterSize = {50}
                buttonWrapStyle = {{marginLeft: 10}}
              />
              <RadioButtonLabel
                obj={obj}
                index={index}
                labelHorizontal={true}
                onPress={handleOnPress}
                labelStyle={{fontSize: 20, color: '#000'}}
                />
            </RadioButton>
          ))
        }
      </RadioForm>
    </View>
  )
}


mapDispatchToProps = (dispatch) => {
  return {
    setIsThere: (isThere) => {
      dispatch(addIsThere(isThere));
    }
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-evenly'
  }
})
export default connect(null, mapDispatchToProps)(AddIsThere);