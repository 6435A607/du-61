import React from 'react';
import { Text, TouchableHighlight, View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { removeAllChipotle } from './actions';

function DeleteAllBoundary(props) {
  handleOnPress = () => {
    props.removeAllChipotle();
  }
  return(
    <View style = {styles.view}>
      <TouchableHighlight
        underlayColor = 'lightskyblue'
        onPress={handleOnPress}>
        <View style = {styles.button}>
          <Text style={styles.text}>Очистить список</Text>
        </View>
      </TouchableHighlight>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'deepskyblue',
    justifyContent: 'center',
    height: '10%',
    width: '75%',
    alignSelf: 'center',
    borderRadius: 20,
    borderWidth: 3,
    marginBottom: '5%'
  },

  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height: "100%",
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white'
  }
})

const mapDispatchToProps = (dispatch) => {
  return {
    removeAllChipotle: () => {
      dispatch(removeAllChipotle());
    }
  }
}

export default connect(null, mapDispatchToProps)(DeleteAllBoundary);