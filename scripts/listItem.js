import React from 'react';
import { connect } from 'react-redux';
import { removeChipotle } from './actions';
import { View, Text, Alert, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'native-base';


function ListItem(props) {
  const handleOnPress = () => {
    Alert.alert(null,
      "Удалить элемент", [ 
        { text: 'Да', onPress: () => props.remove(props.item.id)},
        { text: 'Нет', style: 'cancel' }
      ],
      { canceble: false });
    return true; 
  }
  return (
    <View style={styles.flatitem}>
      <View>
        {/* Пустой компонент, чтобы нормально разместить содержимое по центру */}
      </View>
      <View>
        <Text style={styles.text}>{`${props.item.km} км ${props.item.pk} пк ${props.item.speed} км/ч `}</Text>
      </View>
      <TouchableWithoutFeedback onPress={handleOnPress}>
        <Icon type='Fontisto' name='trash' />
      </TouchableWithoutFeedback>
    </View>
  );
}


const styles = StyleSheet.create({
  flatitem: {

    borderBottomWidth: 2,
    fontSize: 20,
    padding: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    alignSelf: 'center'
  }
})

mapDispatchToProps = (dispatch) => {
  return {
    remove: (index) => {
      dispatch(removeChipotle(index));
    }
  }
}

export default connect(null, mapDispatchToProps)(ListItem)