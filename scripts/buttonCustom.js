import React from 'react';
import { View, TouchableHighlight, Text } from 'react-native';

export default function ButtonCustom(props) {
  return(
    <View style={props.styleContainer}>
    <TouchableHighlight
      style={props.styleButton}
      underlayColor={props.underlayColor}
      onPress={props.onPress}>
      <View>
        <Text style={props.styleText}>{props.label}</Text>
      </View>
    </TouchableHighlight>
  </View>
  )
}