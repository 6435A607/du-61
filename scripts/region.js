import React from 'react';
import { View, StyleSheet } from 'react-native';
import PickerCustom from './customPicker';


export default function Region(props) {
  let listRegions = ["Москва", "Петрозаводск"];
  return (
    <View>
      <PickerCustom stylesContainer = {styles.container} stylesPicker={styles.picker} 
        stylesText={styles.text}
        label={"Направление"} onValueChange={props.setRegion} listPickerItems={listRegions} />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'column-reverse',
    width: '85%',
    alignSelf: 'center',
  },
  picker: {
    borderStyle: "solid",
    borderWidth: 2,
    borderRadius: 15,
    textAlign: "center",
    marginHorizontal: '5%',
    fontSize: 20
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: '5%'
  }
})