import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import DeleteAllBoundary from './buttonDeleteAll';
import ListItem from './listItem';

function List(props) {
  const data = props.data;
  const renderItem = ({ item, index }) => (
    <ListItem item={item} index={index} />
  );

  return (
    <View style={styles.view}>
      <FlatList
        style={styles.flatList}
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
      <DeleteAllBoundary />
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    borderTopWidth: 2
  },
})

const mapStateToProps = (state) => {
  return {
    data: state.chipotle
  }
}

export default connect(mapStateToProps)(List);

