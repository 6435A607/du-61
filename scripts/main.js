import React from 'react';
import { View, StyleSheet } from 'react-native';
import GPS from './gps';

export default function Main() {
  return(
    <View style={styles.container}>
      <GPS />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: 'whitesmoke',
  }
})