import React from 'react';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import { Alert, View, Text, ToastAndroid, Button, BackHandler, StyleSheet, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { removeChipotle, removeAllChipotle } from './actions';
import Tts from 'react-native-tts';
import ButtonCustom from './buttonCustom';
import AddPoint from './addPoint';
import Boundary, { Events } from 'react-native-boundary';

class GPS extends React.Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);

    BackgroundGeolocation.configure({
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY, //Desired accuracy in meters. Possible values [HIGH_ACCURACY, MEDIUM_ACCURACY, LOW_ACCURACY, PASSIVE_ACCURACY]. Accuracy has direct effect on power drain. Lower accuracy = lower power drain.	all	MEDIUM_ACCURACY
      notificationTitle: 'Геолокация',
      notificationText: '',
      startOnBoot: false,
      stopOnTerminate: false,
      interval: 500,
      fastestInterval: 500,
      activitiesInterval: 500,
      stopOnStillActivity: false,
      debug: false, //звуки отладочные
    });

    Boundary.on(Events.ENTER, (id) => {
      id.forEach(e => {
        let speakInfo = this.props.chipotle.find((item) => item.id == e);
        Tts.speak(`Внимание! ${speakInfo.km}й километр, пикет ${speakInfo.pk}й, скорость ${speakInfo.speed}`);
        this.props.removeChipotle(e);
      })
    });


    BackgroundGeolocation.on('error', (error) => {
      Alert.alert(error);
      console.log('[ERROR] BackgroundGeolocation error:', error);
    });

    BackgroundGeolocation.on('start', () => {
      Alert.alert("Отслеживание координат запущено");
      console.log('[INFO] BackgroundGeolocation service has been started');
    });

    BackgroundGeolocation.on('stop', () => {
      Alert.alert("Отслеживание координат остановлено");
      console.log('[INFO] BackgroundGeolocation service has been stopped');
    });

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status);
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
            { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
          ]), 1000);
      }
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    BackgroundGeolocation.removeAllListeners();
    Boundary.off(Events.ENTER);
  }

  backPressed = () => {
    Alert.alert(
      "Выход",
      "Братан, ты решил выйти?",
      [
        { text: 'Нет', style: 'cancel' },
        {
          text: 'Да', onPress: () => {
            BackgroundGeolocation.stop();
            BackgroundGeolocation.removeAllListeners();
            this.props.removeAllChipotle();
            BackHandler.exitApp();
          }
        },
      ],
      { cancelable: false });
    return true;
  }

  handlePressStart() {
    BackgroundGeolocation.checkStatus(status => {
      status.locationServicesEnabled ? BackgroundGeolocation.start() : Alert.alert("Включи GPS")
    })
  }

  handlePressStop() {
    BackgroundGeolocation.stop();
  }

  render() {
    return (
      <>
        <View style={styles.buttonPanel}>
          <ButtonCustom styleContainer={styles.buttonContainer}
            styleButton={styles.button}
            styleText={styles.buttonText}
            underlayColor='lightskyblue'
            onPress={this.handlePressStart} label="START" />
          <ButtonCustom styleContainer={styles.buttonContainer}
            styleText={styles.buttonText}
            styleButton={styles.button}
            underlayColor='lightskyblue'
            onPress={this.handlePressStop} label="STOP" />
        </View>

        <AddPoint />
      </>
    );
  }
}

const styles = StyleSheet.create({
  buttonPanel: {
    flexDirection: 'row',
    flex: 0.3,
    alignItems: 'center',

  },

  buttonContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },

  button: {
    backgroundColor: 'deepskyblue',
    height: '50%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    borderWidth: 3
  },

  buttonText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold'
  }

})

const fromStateToProps = (state) => {
  return {
    chipotle: state.chipotle
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    removeChipotle: (chipotle) => {
      dispatch(removeChipotle(chipotle));
    },
    removeAllChipotle: () => {
      dispatch(removeAllChipotle());
    }
  }
}

export default connect(fromStateToProps, mapDispatchToProps)(GPS)