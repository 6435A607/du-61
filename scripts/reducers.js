import { ADD_CHIPOTLE, REMOVE_ALL_CHIPOTLE, REMOVE_CHIPOTLE, SET_IS_THERE } from './types';
import Boundary from 'react-native-boundary';

const initialState = {
  chipotle: [],
  isThere: -1,
};

const chipotleReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CHIPOTLE:
      let chipotle = action.payload;
      Boundary.add({
        lat: chipotle.lat,
        lng: chipotle.lng,
        radius: chipotle.radius,
        id: chipotle.id
      });
      return {
        ...state, chipotle: [...state.chipotle, chipotle]
      };

    case REMOVE_ALL_CHIPOTLE:
      Boundary.removeAll();
      return {
        ...state, chipotle: action.payload
      };

    case REMOVE_CHIPOTLE:
      let newChipotle = state.chipotle; //Из списка удаляем по индексу и вновь заливаем
      let index = newChipotle.findIndex(item => item.id == action.payload);
      Boundary.remove(action.payload);
      const AMOUNT_DELETED_chipotleS = 1
      newChipotle.splice(index, AMOUNT_DELETED_chipotleS);
      return {
        ...state, chipotle: [...newChipotle]
      };

    case SET_IS_THERE:
      return {
        ...state, isThere: action.payload
      }

    default: return state;
  }
}

export default chipotleReducer;