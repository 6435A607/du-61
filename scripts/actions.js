import { ADD_CHIPOTLE, REMOVE_ALL_CHIPOTLE, REMOVE_CHIPOTLE, SET_IS_THERE } from './types';

export const addChipotle = (chipotle) => {
  return {
    type: ADD_CHIPOTLE,
    payload: chipotle
  }
}

export const removeAllChipotle = () => {
  return {
    type: REMOVE_ALL_CHIPOTLE,
    payload: []
  }
}

export const removeChipotle = (chipotle) => {
  return {
    type: REMOVE_CHIPOTLE,
    payload: chipotle
  }
}


export const addIsThere = (isThere) => {
  return {
    type: SET_IS_THERE,
    payload: isThere
  }
}