import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Picker } from 'native-base';

export default function PickerCustom(props) {

  let listPickerItems = props.listPickerItems.map((i, index) =>
    <Picker.Item label={`${i}`} key={index} value={index} />);
  
  return (
    <View style={props.stylesContainer}>
      <View style={props.stylesPicker}>
        <Picker
          enable={true}
          mode='dialog'
          onValueChange={props.onValueChange} >
          {listPickerItems}
        </Picker>
      </View>
      <Text style={props.stylesText}>{props.label}</Text>
    </View>
  )
}
