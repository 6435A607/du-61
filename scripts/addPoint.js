import React, { useState } from 'react';
import { View, StyleSheet, Alert, } from 'react-native';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';

import { connect } from 'react-redux';
import listPoints, { listPointsMoscow } from './listPoints';
import { addChipotle } from './actions';
import AddWarningKm from './addWarningKm';
import AddIsThere from './isThere';
import PanelSetKmPkSpeed from './panelSetKmPkSpeed';
import ButtonCustom from './buttonCustom';
import Region from './region';

function AddPoint(props) {
  const [km, setKm] = useState(); //Индекс, под которым в массиве списка координат будет искатсья километр
  const [pk, setPk] = useState(); //Тоже индекс, но будет корректироваться, т.к. первый индекс 0
  const [speed, setSpeed] = useState(0);
  const [region, setRegion] = useState(0);
  const [warningKm, setWarningKM] = useState(0);

  const handleOnPressButton = () => {
    //Проверки на ошибки
    if (km === undefined || speed === undefined || pk === undefined) {
      Alert.alert("Введи все данные");
      return
    }

    BackgroundGeolocation.checkStatus((e) => {
      e.locationServicesEnabled ? addPoint() : Alert.alert("Включи GPS");
    })
  }

  const addPoint = () => {
    //id = (km - 0.5 + x*0.5 + xy) 
    //В зависимости от направления (туда х = -1, обратно х = 1) будет меняться сумма 0.5 (-1 туда, 0 обратно)
    //у - за сколько км предупреждать. 
    const CORRECTION = 0.5;
    const SPEED_CORRECTION = 5;
    let isThere = props.isThere;
    let id = km - CORRECTION + (isThere * CORRECTION) + (isThere * warningKm);
    try {
      let point = listPoints[region][id];
      let radius = 10000 //Radius default
      let chipotle = {
        lat: point.lat,
        lng: point.lng,
        speed: speed*SPEED_CORRECTION,
        id: point.id.toString(),
        km: listPoints[region][km].id/1000,
        radius: radius,
        pk: pk + CORRECTION*2
      }
      //console.log(`Добавлено ${chipotle.id}`);
      props.add(chipotle);
      Alert.alert("Добавлено предупреждение",
        `${chipotle.km} км ${chipotle.pk} пк скорость: ${speed*SPEED_CORRECTION} км/ч`);
    } catch (error) { Alert.alert("Неправильный ввод данных") }

  }

  return (
    <View style={styles.container} >
      <PanelSetKmPkSpeed
        speed={speed} setSpeed={setSpeed}
        setKm={setKm} setPk={setPk} region={listPoints[region]}
      /> 
      <AddIsThere />
      <AddWarningKm setWarningKM={setWarningKM} /> 
      <Region setRegion={setRegion} />
      <ButtonCustom styleContainer={styles.containerButton}
        styleButton={styles.button}
        styleText={styles.buttonText}
        underlayColor='lightblue'
        onPress={handleOnPressButton}
        label="ДОБАВИТЬ" />

    </View>
  );
}

mapStateToProps = (state) => {
  return {
    isThere: state.isThere
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    add: (chipotle) => {
      dispatch(addChipotle(chipotle));
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: 'space-between',
    flex: 1,
  },
  containerButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '75%',
    height: '15%',
    alignSelf: 'center',
    marginBottom: '10%'
  },
  button: {
    backgroundColor: 'deepskyblue',
    width: '100%',
    height: '100%',
    borderWidth: 3,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddPoint);