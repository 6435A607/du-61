import React, { useState } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { useWindowDimensions, StyleSheet } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

import Main from './scripts/main';
import List from './scripts/list';
import chipotleReducer from './scripts/reducers';

const store = createStore(chipotleReducer);

function App() {
  const layout = useWindowDimensions();   //Настройки двух экранов
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'main', title: 'Main' },   
    { key: 'list', title: 'List' },
  ]);

  const renderScene = SceneMap({    //Какие экраны отрисовываются
    main: Main,
    list: List,
  });

  return (
    <Provider store={store}>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={() => null}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
        style={styles.tabView}
      />
    </Provider>
  );
}

const styles = StyleSheet.create({
  tabView: {

  }
})

export default App;